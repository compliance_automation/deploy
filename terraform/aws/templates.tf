////////////////////////////////
// Templates

# Template vars are conditionally set via the `event-stream-enabled` variable.
# If true, seeds in the appropriate Chef Automate information. If false, launches the stock supervisor.
data "template_file" "sup_service" {
  template = "${file("${path.module}/../templates/hab-sup.service")}"

  vars {
    stream_env = "${var.event-stream-enabled == "true" ? var.event-stream-env-var : ""}"
    flags = "${var.event-stream-enabled == "true" ? "--auto-update  --event-stream-application=${var.event-stream-application} --event-stream-environment=${var.event-stream-environment} --event-stream-site=${var.aws_region} --event-stream-url=${var.automate_ip}:4222 --event-stream-token=${var.automate_token}" : "--auto-update " }"
  }
}

data "template_file" "install_hab" {
  template = "${file("${path.module}/../templates/install-hab.sh")}"

  vars {
    opts = "${var.hab_install_opts}"
  }
}

data "template_file" "audit_toml_linux" {
  template = "${file("${path.module}/../templates/audit_linux.toml")}"

  vars {
    automate_url = "${var.automate_url}"
    automate_token = "${var.automate_token}"
    automate_user = "${var.automate_user}"
  }
}

data "template_file" "config_toml_linux" {
  template = "${file("${path.module}/../templates/config_linux.toml")}"

  vars {
    automate_url = "${var.automate_url}"
    automate_token = "${var.automate_token}"
    automate_user = "${var.automate_user}"
    waiver_env = "none"
    waiver_source_location = "none"
    audit_package = "none"
  }
}

data "template_file" "config_toml_linux_dev" {
  template = "${file("${path.module}/../templates/config_linux.toml")}"

  vars {
    automate_url = "${var.automate_url}"
    automate_token = "${var.automate_token}"
    automate_user = "${var.automate_user}"
    waiver_env = "dev"
    waiver_source_location = "${var.waiver_source_location}"
    audit_package = "${var.audit_package}"
  }
}

data "template_file" "config_toml_linux_prod" {
  template = "${file("${path.module}/../templates/config_linux.toml")}"

  vars {
    automate_url = "${var.automate_url}"
    automate_token = "${var.automate_token}"
    automate_user = "${var.automate_user}"
    waiver_env = "prod"
    waiver_source_location = "${var.waiver_source_location}"
    audit_package = "${var.audit_package}"
  }
}

data "template_file" "pipeline_compliance_env" {
  template = "${file("${path.module}/../templates/compliance_env.env")}"
  vars {
    automate_url = "${var.automate_url}"
    automate_token = "${var.automate_token}"
    automate_user = "${var.automate_user}" 
  }
}