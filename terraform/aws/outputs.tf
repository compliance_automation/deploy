output "cw_prod_public_ip" {
  value = "${join(",", aws_instance.cw_prod.*.public_ip)}"
}
output "cw_dev_public_ip" {
  value = "${join(",", aws_instance.cw_dev.*.public_ip)}"
}

output "gitlab_runner_public_ip" {
  value = "${aws_instance.gitlab_runner.*.public_ip}"
}
