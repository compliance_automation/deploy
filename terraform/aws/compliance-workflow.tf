resource "aws_instance" "gitlab_runner" {
  connection {
    user        = "${var.aws_ami_user}"
    private_key = "${file("${var.aws_key_pair_file}")}"
  }

  ami                         = "${data.aws_ami.centos.id}"
  instance_type               = "${var.test_server_instance_type}"
  key_name                    = "${var.aws_key_pair_name}"
  subnet_id                   = "${aws_subnet.compliance_workflow_subnet.id}"
  vpc_security_group_ids      = ["${aws_security_group.compliance_workflow.id}", "${aws_security_group.habitat_supervisor.id}"]
  associate_public_ip_address = true
  availability_zone           = "us-west-2a"
  count                       = "${var.deploy_gitlab_runner ? 1 : 0}"

  tags {
    Name          = "gitlab_runner_${random_id.instance_id.hex}"
    X-Dept        = "${var.tag_dept}"
    X-Customer    = "${var.tag_customer}"
    X-Project     = "${var.tag_project}"
    X-Application = "${var.tag_application}"
    X-Contact     = "${var.tag_contact}"
    X-TTL         = "${var.tag_ttl}"
  }

  provisioner "file" {
    content     = "${data.template_file.install_hab.rendered}"
    destination = "/tmp/install_hab.sh"
  }

  provisioner "file" {
    content     = "${data.template_file.sup_service.rendered}"
    destination = "/home/${var.aws_ami_user}/hab-sup.service"
  }

  provisioner "file" {
    content     = "${data.template_file.audit_toml_linux.rendered}"
    destination = "/home/${var.aws_ami_user}/audit_linux.toml"
  }

  provisioner "file" {
    content     = "${data.template_file.config_toml_linux.rendered}"
    destination = "/home/${var.aws_ami_user}/config_linux.toml"
  }

  provisioner "file" {
    content = "${data.template_file.pipeline_compliance_env.rendered}"
    destination = "/home/${var.aws_ami_user}/compliance_env.env"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo rm -rf /etc/machine-id",
      "sudo systemd-machine-id-setup",
      "sudo hostname gitlab-runner",
      "sudo groupadd hab",
      "sudo adduser hab -g hab",
      "chmod +x /tmp/install_hab.sh",
      "sudo /tmp/install_hab.sh",
      "sudo hab license accept",
      "sudo hab pkg install ${var.hab-sup-version}",
      "sudo mv /home/${var.aws_ami_user}/hab-sup.service /etc/systemd/system/hab-sup.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl start hab-sup",
      "sudo systemctl enable hab-sup",
      "sleep ${var.sleep}",
      "sudo mkdir -p /hab/user/config-baseline/config /hab/user/audit-baseline/config",
      "sudo chown hab:hab -R /hab/user",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_redirects=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_redirects=0",
      "sudo cp /home/${var.aws_ami_user}/audit_linux.toml /hab/user/audit-baseline/config/user.toml",
      "sudo cp /home/${var.aws_ami_user}/config_linux.toml /hab/user/config-baseline/config/user.toml",
      "sudo hab svc load ${var.config_origin}/${var.config_package} --group ${var.group} --strategy at-once --channel stable",
      "sudo hab svc load ${var.audit_origin}/${var.audit_package} --group ${var.group} --strategy at-once --channel stable", 
      "curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm",
      "sudo rpm -i gitlab-runner_amd64.rpm",
      "sudo gitlab-runner register --non-interactive --url ${var.gitlab_url} --executor 'shell' --registration-token '${var.gitlab_token}' --description 'Habitat Runner' --tag-list 'Habitat' --run-untagged='false' --locked='false' --access-level='not_protected'",
      "sudo gitlab-runner start",
      "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers",
      "sudo cp /home/${var.aws_ami_user}/compliance_env.env /home/gitlab-runner/compliance_env.env",

    ]

  }
}


# Single Mongdb instance peered with the permanent peer


# National Parks instances peered with the permanent peer and binded to mongodb instance
resource "aws_instance" "cw_prod" {
  connection {
    user        = "${var.aws_ami_user}"
    private_key = "${file("${var.aws_key_pair_file}")}"
  }

  ami                         = "${data.aws_ami.centos.id}"
  instance_type               = "${var.test_server_instance_type}"
  key_name                    = "${var.aws_key_pair_name}"
  subnet_id                   = "${aws_subnet.compliance_workflow_subnet.id}"
  vpc_security_group_ids      = ["${aws_security_group.compliance_workflow.id}", "${aws_security_group.habitat_supervisor.id}"]
  associate_public_ip_address = true
  count                       = "${var.prod_count}"
  availability_zone           = "us-west-2a"

  tags {
    Name          = "prod${count.index}_compliance_workflow${random_id.instance_id.hex}"
    X-Dept        = "${var.tag_dept}"
    X-Customer    = "${var.tag_customer}"
    X-Project     = "${var.tag_project}"
    X-Application = "${var.tag_application}"
    X-Contact     = "${var.tag_contact}"
    X-TTL         = "${var.tag_ttl}"
  }

  provisioner "file" {
    content     = "${data.template_file.install_hab.rendered}"
    destination = "/tmp/install_hab.sh"
  }

  provisioner "file" {
    content     = "${data.template_file.sup_service.rendered}"
    destination = "/home/${var.aws_ami_user}/hab-sup.service"
  }

  provisioner "file" {
    content     = "${data.template_file.audit_toml_linux.rendered}"
    destination = "/home/${var.aws_ami_user}/audit_linux.toml"
  }

  provisioner "file" {
    content     = "${data.template_file.config_toml_linux_prod.rendered}"
    destination = "/home/${var.aws_ami_user}/config_linux.toml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo rm -rf /etc/machine-id",
      "sudo systemd-machine-id-setup",
      "sudo hostname prod-compliance-workflow-${count.index}",
      "sudo groupadd hab",
      "sudo adduser hab -g hab",
      "chmod +x /tmp/install_hab.sh",
      "sudo /tmp/install_hab.sh",
      "sudo hab license accept",
      "sudo hab pkg install ${var.hab-sup-version}",
      "sudo mv /home/${var.aws_ami_user}/hab-sup.service /etc/systemd/system/hab-sup.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl start hab-sup",
      "sudo systemctl enable hab-sup",
      "sleep ${var.sleep}",
      "sudo mkdir -p /hab/user/compliance-config/config /hab/user/compliance-audit/config",
      "sudo chown hab:hab -R /hab/user",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_redirects=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_redirects=0",
      "sudo cp /home/${var.aws_ami_user}/audit_linux.toml /hab/user/compliance-audit/config/user.toml",
      "sudo cp /home/${var.aws_ami_user}/config_linux.toml /hab/user/compliance-config/config/user.toml",
      "sudo hab svc load ${var.config_origin}/${var.config_package} --group ${var.group} --strategy at-once --channel prod",
      "sudo hab svc load ${var.audit_origin}/${var.audit_package} --group ${var.group} --strategy at-once --channel prod",
    ]
  }
}

resource "aws_instance" "cw_dev" {
  connection {
    user        = "${var.aws_ami_user}"
    private_key = "${file("${var.aws_key_pair_file}")}"
  }

  ami                         = "${data.aws_ami.centos.id}"
  instance_type               = "${var.test_server_instance_type}"
  key_name                    = "${var.aws_key_pair_name}"
  subnet_id                   = "${aws_subnet.compliance_workflow_subnet.id}"
  vpc_security_group_ids      = ["${aws_security_group.compliance_workflow.id}", "${aws_security_group.habitat_supervisor.id}"]
  associate_public_ip_address = true
  count                       = "${var.dev_count}"
  availability_zone           = "us-west-2a"

  tags {
    Name          = "dev${count.index}_compliance_workflow${random_id.instance_id.hex}"
    X-Dept        = "${var.tag_dept}"
    X-Customer    = "${var.tag_customer}"
    X-Project     = "${var.tag_project}"
    X-Application = "${var.tag_application}"
    X-Contact     = "${var.tag_contact}"
    X-TTL         = "${var.tag_ttl}"
  }

  provisioner "file" {
    content     = "${data.template_file.install_hab.rendered}"
    destination = "/tmp/install_hab.sh"
  }

  provisioner "file" {
    content     = "${data.template_file.sup_service.rendered}"
    destination = "/home/${var.aws_ami_user}/hab-sup.service"
  }

  provisioner "file" {
    content     = "${data.template_file.audit_toml_linux.rendered}"
    destination = "/home/${var.aws_ami_user}/audit_linux.toml"
  }

  provisioner "file" {
    content     = "${data.template_file.config_toml_linux_dev.rendered}"
    destination = "/home/${var.aws_ami_user}/config_linux.toml"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo rm -rf /etc/machine-id",
      "sudo systemd-machine-id-setup",
      "sudo hostname dev-compliance-workflow-${count.index}",
      "sudo groupadd hab",
      "sudo adduser hab -g hab",
      "chmod +x /tmp/install_hab.sh",
      "sudo /tmp/install_hab.sh",
      "sudo hab license accept",
      "sudo hab pkg install ${var.hab-sup-version}",
      "sudo mv /home/${var.aws_ami_user}/hab-sup.service /etc/systemd/system/hab-sup.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl start hab-sup",
      "sudo systemctl enable hab-sup",
      "sleep ${var.sleep}",
      "sudo mkdir -p /hab/user/compliance-config/config /hab/user/compliance-audit/config",
      "sudo chown hab:hab -R /hab/user",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_source_route=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.default.accept_redirects=0",
      "sudo /sbin/sysctl -w net.ipv4.conf.all.accept_redirects=0",
      "sudo cp /home/${var.aws_ami_user}/audit_linux.toml /hab/user/compliance-audit/config/user.toml",
      "sudo cp /home/${var.aws_ami_user}/config_linux.toml /hab/user/compliance-config/config/user.toml",
      "sudo hab svc load ${var.config_origin}/${var.config_package} --group ${var.group} --strategy at-once --channel dev",
      "sudo hab svc load ${var.audit_origin}/${var.audit_package} --group ${var.group} --strategy at-once --channel dev",
    ]
  }
}