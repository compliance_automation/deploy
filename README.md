# Effortless Compliance Workflow environment deployment
This repo depolys an environment to demo working with Effortless Audit, specifically around waivers and custom controls.

This demo can leverage any Effortless Audit package. However to demo the waiver workflow described in this README, it requires an Effortless Config package with a custom recipe (example in the hardening cookbook in this repo https://gitlab.com/compliance_automation/effortless-compliance-config ).

Full instructions for deploying and deliverying that demo can be found here:

Only support AWS

## Usage

## Deploy:

### Chef Automate

1. Clone this repo
2. `cd terraform/chef-automate/aws`
3. Copy terraform.tfvars.example to terraform.tfvars
4. Modify terraform.tfvars with appropriate values for your A2 instance including:

- automate_hostname
- automate_license
- channel
- disable_event_tls
- all required instance tags(tag_customer, tag_project, tag_name, tag_dept, tag_contact, tag_application, tag_ttl)

5. `terraform apply`
6. Note `chef_automate_public_ip`, `a2_token`, and `a2_admin_password` in terraform output

```
Outputs:

a2_admin = admin
a2_admin_password = <password>
a2_token = <token>
a2_url = https://superSA-automate.chef-demo.com
chef_automate_public_ip = 34.222.124.23
chef_automate_server_public_r53_dns = superSA-automate.chef-demo.com
```

### Demo Nodes

This will deploy demo nodes confiured with an Effortless Config package and running an Effortless Audit package. It will deploy nodes into "dev" and "prod" environments with appropriate environment tags and configurable hab channels for each environment's effortless packages. It will also optionally deploy a gitlab runner to enable upating habitat packages through gitlab CI pipelines. 

1. cd terraform/aws in this repo
2. Copy terraform.tfvars.example to terraform.tfvars
3. Modify terraform.tfvars. There are the relevant variables:

- `prod_count`            #number of nodes to deploy into prod environment
- `dev_count`             #number of nodes to deploy in dev environment
- `automate_url`          #from A2 deployment output
- `automate_hostname`     #from A2 deployment vars
- `automate_ip`           #from A2 deployment output
- `automate_token`        #from A2 deployment output
- `automate_user`         #from A2 deployment output
- `deploy_gitlab_runner`  #set to true to deploy a runner
- `gitlab_token`          #token for the gitlab scope you want ot register the runner to
- `gitlab_url`            #default is public gitlab
- `config_origin`         #origin of the effortless config package to use
- `config_package`        #name of the effortless config package to use
- `audit_origin`          #origin of the effortless audit package to use
- `audit_package`         #name of the effortless audit package to use
- `waiver_source_location` #base url where the waiver toml files are stored

4. `terraform apply`
5. output will display IP addresses for prod, dev, and optional gitlab runner instances
